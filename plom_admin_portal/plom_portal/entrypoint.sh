#!/bin/bash

# SPDX-License-Identifier: FSFAP
# Copyright (C) 2024 Edith Coates

sleep 10  # wait for postgres to start up
python3 manage.py plom_portal_launch --admin-login $ADMIN_USERNAME $ADMIN_PASSWORD
python3 manage.py runserver 0.0.0.0:8000