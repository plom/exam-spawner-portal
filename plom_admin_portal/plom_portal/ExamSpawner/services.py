# SPDX-License-Identifier: FSFAP
# Copyright (C) 2024 Edith Coates

import psycopg2
from typing import Optional, List
import docker
from datetime import datetime

from django.conf import settings

from .models import PostgresDatabase, PlomContainer


def refresh_postgres_db():
    """Connect to the database over tcp/ip, delete it, and re-create it."""
    host = settings.DATABASES["postgres"]["HOST"]

    conn = psycopg2.connect(user="postgres", password="postgres", host=host)
    try:
        conn.autocommit = True
        db_name = settings.DATABASES["default"]["NAME"]

        with conn.cursor() as curs:
            curs.execute("SELECT datname FROM pg_database;")
            database_names = curs.fetchall()
            databases = [t[0] for t in database_names]  # convert a list of single-element tuples into a list of strings

            for db in databases:
                if db == db_name or "plom_" in db:
                    curs.execute(f"DROP DATABASE {db};")
                if db == db_name:
                    curs.execute(f"CREATE DATABASE {db};")
    finally:
        conn.close()


def create_new_database(tag_name: str):
    """Create a new postgres database for a Plom container."""
    host = settings.DATABASES["postgres"]["HOST"]

    conn = psycopg2.connect(user="postgres", password="postgres", host=host)
    try:
        conn.autocommit = True
        db_name = f"plom_{tag_name}"

        with conn.cursor() as curs:
            curs.execute(f"CREATE DATABASE {db_name};")
    finally:
        conn.close()
        container = PlomContainer.objects.get(tag=tag_name)
        database = PostgresDatabase(name=f"plom_{tag_name}", container=container)
        database.save()


def get_databases() -> Optional[List[PostgresDatabase]]:
    """Return a list of databases in Postgres."""
    host = settings.DATABASES["postgres"]["HOST"]

    databases: Optional[List[str]] = None
    try:
        conn = psycopg2.connect(user="postgres", password="postgres", host=host)
        conn.autocommit = True

        with conn.cursor() as curs:
            curs.execute("SELECT datname FROM pg_database;")
            database_names = curs.fetchall()
            databases = [t[0] for t in database_names]  # convert a list of single-element tuples into a list of strings
    
    finally:
        conn.close()
        if databases is None:
            return None
        
        db_names = []
        for db_name in databases:
            container_db_query = PostgresDatabase.objects.filter(name=db_name)
            if container_db_query.exists():
                container_db = container_db_query.first()
                db_names.append(container_db)
        
        return db_names


def delete_database(name: str):
    """Delete a postgres database."""
    host = settings.DATABASES["postgres"]["HOST"]

    try:
        conn = psycopg2.connect(user="postgres", password="postgres", host=host)
        conn.autocommit = True

        with conn.cursor() as curs:
            curs.execute(f"DROP database {name}")
    
    finally:
        conn.close()
        database = PostgresDatabase.objects.get(name=name)
        database.delete()


def get_container_status(tag: str) -> int:
    """Return the status of a Plom container."""
    container_model = PlomContainer.objects.get(tag=tag)

    client = docker.DockerClient(
        base_url="unix://var/run/docker.sock",
        version="1.35",
    )

    status = PlomContainer.ERROR
    try:
        container = client.containers.get(f"plom_container_{tag}")
        if container.status == "running":
            status = PlomContainer.UP
        elif container.status == "exited":
            status = PlomContainer.DOWN
        elif container.status == 'paused':
            status = PlomContainer.DOWN
    finally:
        container_model.status = status
        container_model.last_status_refresh = datetime.now()
        container_model.save()
        return status
