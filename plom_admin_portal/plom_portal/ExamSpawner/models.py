from datetime import datetime

from django.db import models

# Create your models here.

class PlomContainer(models.Model):
    StatusChoices = models.IntegerChoices(
        "status", "UP DOWN ERROR"
    )
    UP = StatusChoices.UP
    DOWN = StatusChoices.DOWN
    ERROR = StatusChoices.ERROR

    tag = models.TextField(null=False, default="", unique=True)
    created = models.DateTimeField(null=False, default=datetime.now)
    last_status_refresh = models.DateTimeField(null=True, default=datetime.now)
    status = models.IntegerField(null=False, choices=StatusChoices.choices, default=DOWN)
    port = models.PositiveIntegerField(null=False, default=8000, unique=True)


class PostgresDatabase(models.Model):
    name = models.TextField(null=False, default="", unique=True)
    created = models.DateField(null=False, default=datetime.now)
    container = models.OneToOneField(PlomContainer, on_delete=models.CASCADE, null=True)
