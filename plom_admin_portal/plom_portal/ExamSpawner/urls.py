from django.urls import path

from . import views

urlpatterns = [
    path("", views.Index.as_view(), name="index"),
    path("login", views.LoginAdmin.as_view(), name="login_admin"),
    path("containers", views.Container.as_view(), name="containers"),
    path("containers/<tag>", views.ContainerDetail.as_view(), name="container_detail"),
    path("containers/<tag>/pause", views.PauseResumeContainer.as_view(), name="pause_resume_container"),
    path("database/<name>", views.DatabaseDetail.as_view(), name="database_detail"),
]