# SPDX-License-Identifier: FSFAP
# Copyright (C) 2024 Edith Coates

from django.contrib.auth.models import User
from django.core.management import call_command
from django.core.management.base import BaseCommand, CommandParser, CommandError

from ExamSpawner.services import refresh_postgres_db


class Command(BaseCommand):
    """Initialize the production portal for spawning Plom servers."""

    def add_arguments(self, parser: CommandParser) -> None:
        parser.add_argument(
            "--admin-login",
            action="store",
            nargs=2,
            help="The username for the admin account."
        )
    
    def handle(self, *args, **options):
        self.stdout.write("Refreshing portal database...")
        refresh_postgres_db()

        call_command("makemigrations")
        call_command("migrate")

        admin_username, admin_password = options["admin_login"]

        if User.objects.filter(is_superuser=True).count() > 0:
            raise CommandError("Cannot initialize server - superuser already exists.")
        
        admin = User.objects.create_superuser(username=admin_username, password=admin_password)
        admin.save()