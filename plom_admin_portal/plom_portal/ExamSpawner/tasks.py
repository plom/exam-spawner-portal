import random
import docker
from celery import shared_task

from django.conf import settings

from ExamSpawner.models import PlomContainer
from ExamSpawner.services import create_new_database, delete_database, get_container_status


@shared_task
def create_container(
    tag: str,
    port: int,
    reuse_admin_login: bool = False,
):
    client = docker.DockerClient.from_env()

    # get the computed postgres host
    postgres = client.containers.get("plom-docker-compose-localhost-postgres-1")
    postgres_host = postgres.attrs["NetworkSettings"]["IPAddress"]

    # create a new container
    client.containers.run(
        settings.SPAWNER_PLOM_IMAGE,
        command=["/bin/bash", "-c", "./docker_run.sh"],
        name=f"plom_container_{tag}",
        detach=True,
        working_dir="/src/plom_server",
        volumes=["/var/run/docker.sock:/var/run/docker.sock"],
        environment=[
            f"PLOM_DATABASE_HOSTNAME={postgres_host}",
            f"PLOM_DB_NAME=plom_db_{tag}",
            f"PLOM_CONTAINER_PORT={port}"
        ],
        network_mode="host",
    )

    new_container = PlomContainer(
        tag=tag,
        status=PlomContainer.UP,
        port=port,
    )
    new_container.save()
    create_new_database(tag)


@shared_task
def pause_resume_container(tag: str):
    client = docker.DockerClient(base_url="unix://var/run/docker.sock", version="1.35")
    container = client.containers.get(f"plom_container_{tag}")
    status = container.status

    if status == "running":
        container.pause()
    else:
        # TODO: use "resume"
        container.unpause()
    
    get_container_status(tag=tag)


@shared_task
def delete_container(tag: str):
    # Instead of delete, archive?
    client = docker.DockerClient(base_url="unix://var/run/docker.sock", version="1.35")
    container = client.containers.get(f"plom_container_{tag}")
    container.stop()
    container.remove()

    delete_database(f"plom_{tag}")
    
    container_model = PlomContainer.objects.get(tag=tag)
    container_model.delete()
