# SPDX-License-Identifier: FSFAP
# Copyright (C) 2024 Edith Coates

from rest_framework.views import APIView
from rest_framework.request import HttpRequest
from rest_framework.response import Response

from django.http import HttpResponseRedirect
from django.urls import reverse

from ..tasks import create_container


class Container(APIView):
    """Handle listing and creating new Plom containers."""

    def get(self, request: HttpRequest) -> Response:
        return Response(template_name="new_container.html")

    def post(self, request: HttpRequest) -> HttpResponseRedirect:
        container_tag = request.POST["container_tag"]
        container_port = request.POST["container_port"]

        create_container.delay(container_tag, container_port, False)
        return HttpResponseRedirect(reverse("index"))
