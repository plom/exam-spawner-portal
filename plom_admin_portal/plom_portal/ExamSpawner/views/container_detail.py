# SPDX-License-Identifier: FSFAP
# Copyright (C) 2024 Edith Coates

from rest_framework.views import APIView
from rest_framework.request import HttpRequest
from rest_framework.response import Response

from django.http import HttpResponseRedirect
from django.urls import reverse

from ..models import PlomContainer
from ..tasks import delete_container, pause_resume_container
from ..services import get_container_status


class ContainerDetail(APIView):
    """Handle viewing and deleting specific Plom containers."""

    def get(self, request: HttpRequest, tag: str) -> Response:
        container = PlomContainer.objects.get(tag=tag)
        status_int = get_container_status(tag=tag)
        if status_int == PlomContainer.UP:
            status = "running"
        elif status_int == PlomContainer.DOWN:
            status = "down"
        else:
            status = "error"

        return Response(
            {
                "tag": tag,
                "container": container,
                "status": status,
            },
            template_name="container_detail.html",
        )

    def post(self, request: HttpRequest, tag: str) -> HttpResponseRedirect:
        delete_container.delay(tag)
        return HttpResponseRedirect(reverse("index"))


class PauseResumeContainer(APIView):
    """Handle pausing and resuming Plom containers."""

    def post(self, request: HttpRequest, tag: str) -> HttpResponseRedirect:
        pause_resume_container.delay(tag)
        return HttpResponseRedirect(reverse("container_detail", args=(tag,)))
