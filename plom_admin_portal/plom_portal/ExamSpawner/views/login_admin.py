# SPDX-License-Identifier: FSFAP
# Copyright (C) 2024 Edith Coates

from rest_framework.views import APIView
from rest_framework.request import HttpRequest
from rest_framework.permissions import AllowAny

from django.contrib.auth import authenticate, login
from django.http import HttpResponseRedirect
from django.urls import reverse


class LoginAdmin(APIView):
    """Sign in the administrator from the browser."""
    permission_classes = [AllowAny]

    def post(self, request: HttpRequest) -> HttpResponseRedirect:
        username = request.POST["admin_username"]
        password = request.POST["admin_password"]

        admin = authenticate(request=request, username=username, password=password)
        if admin is not None:
            login(request, admin)
        return HttpResponseRedirect(reverse("index"))
