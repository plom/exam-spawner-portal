# SPDX-License-Identifier: FSFAP
# Copyright (C) 2024 Edith Coates

from rest_framework.views import APIView
from rest_framework.request import HttpRequest
from rest_framework.response import Response
from rest_framework.permissions import AllowAny

from ..models import PlomContainer
from ..services import get_databases


class Index(APIView):
    """The landing page of the admin portal."""
    permission_classes = [AllowAny]

    def get(self, request: HttpRequest) -> Response:
        n_containers = PlomContainer.objects.all().count()
        containers = PlomContainer.objects.all()

        database_names = get_databases()
        n_databases = len(database_names)

        return Response(
            {
                "n_containers": n_containers,
                "containers": containers,
                "databases": database_names,
                "n_databases": n_databases
            },
            template_name="index.html"
        )