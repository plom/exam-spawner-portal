# SPDX-License-Identifier: FSFAP
# Copyright (C) 2024 Edith Coates

from rest_framework.views import APIView
from rest_framework.request import HttpRequest
from rest_framework.response import Response

from django.http import HttpResponseRedirect
from django.urls import reverse

from ..models import PostgresDatabase


class DatabaseDetail(APIView):
    """Handle viewing and deleting specific Plom databases."""

    def get(self, request: HttpRequest, name: str) -> Response:
        database = PostgresDatabase.objects.get(name=name)
        return Response(
            {
                "name": name,
                "database": database
            },
            template_name="database_detail.html",
        )

    # def post(self, request: HttpRequest, tag: str) -> HttpResponseRedirect:
    #     delete_container.delay(tag)
    #     return HttpResponseRedirect(reverse("index"))
