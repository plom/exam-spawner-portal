# Plom Exam Spawner
Run multiple production Plom servers from one place with this repository. This portal includes an admin interface for creating new Plom servers, handling initial signups of managers, storing data, and hosting Plom exams over HTTPS.

## Installation
The portal requires Docker and Docker Compose. To install and run:
```
git clone https://gitlab.com/plom/exam-spawner-portal.git
cd exam-spawner-portal
docker compose up
```
Note, there's no dash in `docker compose` because the code is using Compose V2.