# SPDX-License-Identifier: FSFAP
# Copyright (C) 2023 Edith Coates
# Copyright (C) 2023 Colin B. Macdonald

if ! command -v docker compose up &> /dev/null
then
    echo "Unable to run docker compose. Is Docker installed on your system?"
    exit 1
fi

if ! command -v git --version &> /dev/null
then
    echo "Unable to run git. Is it installed on your system?"
    exit 1
fi

git clone https://gitlab.com/ecoates100/plom-docker-compose-localhost.git
cd plom-docker-compose-localhost
docker compose up